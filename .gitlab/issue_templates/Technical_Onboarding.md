### Overview

The [Sec Section](https://about.gitlab.com/direction/security/) encompasses the [Protect](https://about.gitlab.com/handbook/engineering/development/protect/) and [Secure](https://about.gitlab.com/handbook/engineering/development/secure/) stages.

The features provided by these teams are mostly leveraging tools that are executed during pipelines,
using Docker images.
That’s why it’s crucial to set up a development environment with the [GitLab CI Runner](https://gitlab.com/gitlab-org/gitlab-runner).

This document will guide you through the whole process. Don't hesitate to ask questions in Slack if anything is unclear:

- `#sec-section` for things relevant to the Protect and Secure Stages
- `#s_protect` for things related to the Protect Stage
- `#s_secure` for things related to the Secure Stage
- `#questions` for any general questions
- `#gdk` for [GDK](https://gitlab.com/gitlab-org/gitlab-development-kit) related questions

Enjoy!

Need some help? Ping one of your [Sec team mates](https://about.gitlab.com/company/team/?department=sec-section), I'm sure they will be more than willing to answer your call!

#### General

1. [ ] Review the relevant handbook pages to understand how we fit in the organization structure, and what product categories we're responsible for developing and maintaining:
   1. [ ] [Direction for the Sec Section](https://about.gitlab.com/direction/security/)
   1. [ ] [Product Categories Hierarchy](https://about.gitlab.com/handbook/product/product-categories/#hierarchy) and find our Section, Stages, Groups and their respective Categories.
   1. [ ] [Secure and Protect Glossary of Terms](https://about.gitlab.com/handbook/engineering/development/secure/glossary-of-terms/) to get used to the terms you will frequently hear.

<details>
<summary>Protect</summary>

1. [ ] [Protect sub-department](https://about.gitlab.com/handbook/engineering/development/protect/)
1. [ ] [Protect Stage](https://about.gitlab.com/handbook/product/product-categories/#protect-stage)

</details>

<details>
<summary>Secure</summary>

1. [ ] Read about GitLab [Secure features, how they are set up and work](https://docs.gitlab.com/ee/user/application_security/)
   1. [ ] [Secure Stage](https://about.gitlab.com/handbook/product/product-categories/#secure-stage)
1. [ ] Optionally bookmark [repositories maintained by the Secure team](https://gitlab.com/gitlab-org/security-products).
1. [ ] The [Secure Group](https://gitlab.com/gitlab-org/secure) is another group which is used to address members of the Secure team and to keep other projects which do not directly affect the product. For instance the [onboarding](https://gitlab.com/gitlab-org/secure/onboarding).

</details>

### Day 6: Setup

#### Accounts

1. [ ] Manager: Add the new member to the corresponding team group in [https://gitlab.com/groups/gitlab-org/secure](https://gitlab.com/groups/gitlab-org/secure) as `Maintainer` (this is mostly used for mentions)
1. [ ] Manager: Add the new member to [Geekbot](https://geekbot.io/)
1. [ ] New team member: Please request a [license](https://about.gitlab.com/handbook/developer-onboarding/#working-on-gitlab-ee-developer-licenses) to be able to work on GitLab EE features. Ensure to ask for an "Ultimate" license type, so that all features including the Security Dashboard are enabled. You should receive an email with a license file that you will use during GDK setup.
1. [ ] New team member: Create a [new access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request) using the `Individual Bulk Access Request` template for the following items:
    - **Periscope** with `Editor` access level. You must sign into Periscope via Okta before your permissions can be granted. The first time you sign in, your account will be initialized.
    - **Slack User Group** pick the appropriate one in:
      - Protect: `@protect_container_security_be`, `@protect_container_security_fe`
      - Secure: `@secure_composition_analysis_be`, `@secure_dynamic_analysis_be`, `@secure_static_analysis_be`, `@secure_threat_insights_be`, `@secure_fe`, `@secure_threat_insights_fe`
1. [ ] New team member: Sign-in on [Sentry](https://sentry.gitlab.net/gitlab/) and join the appropriate team.
1. [ ] New team member: Say hello to the Sec team by introducing yourself on Slack channel #sec-section. 

<details>
<summary>Protect</summary>

1. [ ] Manager: Add the new member to the [Protect Stage Calendar](https://calendar.google.com/calendar/u/0?cid=Z2l0bGFiLmNvbV9lZDYyMDd1ZWw3OGRlMGoxODQ5dmpqbmIza0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t)
1. [ ] Manager: Add the new member to the [defend-be google group](https://groups.google.com/a/gitlab.com/g/defend-be/members). This grants them access to the Google Cloud Platform [group-defend project](https://console.cloud.google.com/home/dashboard?folder=&organizationId=&project=group-defend-c8e44e).
1. [ ] Manager: Add the new member to [https://staging.gitlab.com/groups/defend-team-test](https://staging.gitlab.com/groups/defend-team-test) as Maintainer.
1. [ ] Manager: Add the new member to the corresponding team's subgroup:
    - Container Security Backend: https://gitlab.com/gitlab-org/protect/container-security-backend
    - Container Security Frontend: https://gitlab.com/gitlab-org/protect/container-security-frontend

</details>

<details>
<summary>Secure</summary>

1. [ ] Manager: Add the new member to [https://staging.gitlab.com/groups/secure-team-test](https://staging.gitlab.com/groups/secure-team-test) as Maintainer
1. [ ] Manager: Add the new member to the [Secure Stage Calendar](https://calendar.google.com/calendar/r/settings/calendar/Z2l0bGFiLmNvbV9tZDBhbzM2Z3B2bDV2MWY0MTI4ZXJobmo2Z0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t) and explain what it is.
1. [ ] Manager: Add the new member to [https://gitlab.com/groups/gitlab-org/secure/-/group_members](https://gitlab.com/groups/gitlab-org/secure/-/group_members)
1. [ ] Manager: Add the new member to the corresponding team's subgroup:
    - Composition Analysis: https://gitlab.com/gitlab-org/secure/composition-analysis-be as `Developer`
    - Dynamic Analysis: https://gitlab.com/gitlab-org/secure/dynamic-analysis-be as `Maintainer`
    - Static Analysis: https://gitlab.com/gitlab-org/secure/static-analysis-be as `Developer`
    - Secure Frontend: https://gitlab.com/gitlab-org/secure/frontend as `Maintainer`
    - Threat Insights Backend: https://gitlab.com/groups/gitlab-org/secure/threat-insights-backend-team as  `Maintainer`
    - Threat Insights Frontend: https://gitlab.com/groups/gitlab-org/secure/threat-insights-frontend-team as  `Maintainer`

</details>

#### Docker
1. [ ] Install [a Docker Desktop alternative](https://about.gitlab.com/handbook/tools-and-tips/mac/#docker-desktop) (Confidential Issue)
   1. [ ] If a more official recommendation has been made, open a Merge Request to update this template.

### Development Setup

You can choose to either or both of these. Note that GCK runs better on Linux than macOS.

1. [ ] [GDK (Gitlab Development Kit)](https://gitlab.com/gitlab-org/gitlab-development-kit/-/tree/main) provides a standard development environment commonly used by Gitlab engineers.

    <details>
    <summary>GDK</summary>

    1. [ ] GDK
        1. [ ] Install all needed [dependencies for GDK](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/index.md). Watch [video on GDK installation](https://www.youtube.com/watch?v=gxn-0KSfNaU).
        1. [ ] Change your local GitLab hostname to `gitlab.localdev`
            1. [ ] Add `127.0.0.1 gitlab.localdev` line to `/etc/hosts` file. This creates an alias for `127.0.0.1` to `gitlab.localdev`.
            1. [ ] Add `hostname: gitlab.localdev` to `gdk.yml`
            1. [ ] Run `gdk reconfigure` and then `gdk restart`. After this, you should be able to access your local GitLab instance at `http://gitlab.localdev:3000`.
        1. [ ] Visit http://gitlab.localdev:3000/users/sign_in and login as an admin user with username `root` and password `5iveL!fe`. You will need to change the password after your first login.
        1. [ ] Upload your Ultimate license key for GitLab Enterprise Edition by visiting `http://gitlab.localdev:3000/admin/license/new`. If you do not have GitLab Ultimate license, follow instructions [here](https://about.gitlab.com/handbook/developer-onboarding/#working-on-gitlab-ee-developer-licenses) to obtain a license file.
        1. [ ] If you’re stuck, ask for help in `#development`, `#gdk` Slack channels. Use Slack search for your questions.
               first with filter `in:#gdk`. There is a possibility that someone has already had a similar issue.
        1. [ ] Check [How to use GitLab Development Kit doc](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/main/doc/howto/index.md)
        1. [ ] You’ll need to know [commands](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/main/HELP) to operate your local environment successfully. For example, to start database locally (to run tests), you need to start db with `gdk start postgresql redis`. For our purposes, run everything excluding the runners with `gdk start && gdk stop runner` because we'll run those separately in the steps below.
        1. [ ] GDK contains a collection of resources that help running an instance of GitLab locally as well as
               GitLab codebase itself. The GitLab code can be found in `/gitlab` folder of GDK. Check this folder.
        1. [ ] If you have not already, create a reminder for 3 months out from your start date to add yourself as a reviewer for the gitlab project by adding a [`projects` key](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/ea26398651a41e9eae631efdb266a4b9954e910f/data/team.yml#L20128-20129) to your entry in gitlab.com's `data/team.yml`. This will allow you to be automatically suggested as a reviewer for MRs.
    1. [ ] Runner
        1. [ ] Install GitLab Runner locally with [this tutorial](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/main/doc/howto/runner.md).
               Be sure to follow the Docker related setup instructions. (Optionally: you can setup your Runners to run inside containers by following these [instructions](https://docs.gitlab.com/runner/install/docker.html#docker-image-installation-and-configuration))
        1. [ ] Register your runner with the command: `gitlab-runner register -c <gdk-path>/gitlab-runner-config.toml`,
               choose `Docker` as an executor and http://gitlab.localdev:3000/ as the coordinator URL. This command will generate the `gitlab-runner-config.toml` file.
        1. [ ] Add your runner `token` and set `extra_hosts` inside `gdk.yml`. It should have the following contents with your runner's token in place of `<token>`:
            ```yml
            hostname: gitlab.localdev
            runner:
              token: <runner-token>
              extra_hosts:
                - gitlab.localdev:192.168.99.1
            ```
        1. [ ] Run `gdk reconfigure`.
        1. [ ] Run the GitLab Runner in the foreground with the command:
        `gitlab-runner --log-level debug run --config <gdk-path>/gitlab-runner-config.toml`. If your Runner is setup to run inside docker container then run `docker run gitlab/gitlab-runner --log-level debug run --config <gdk-path>/gitlab-runner-config.toml`
        1. [ ] Create a [Loopback Interface](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/runner.md#set-up-docker-and-gdk) to make sure docker and the gdk find each other. Also, ensure to remove any previous mapping for `gitlab.localdev` before mapping it to `172.16.123.1` inside `/etc/hosts` file.
        1. [ ] Putting it all together, your final `gdk.yml` file should look something like this:
            ```yml
            hostname: gitlab.localdev
            runner:
              enabled: true
              executor: docker
              install_mode: docker
              token: <runner-token>
              extra_hosts: ["gdk.test:172.16.123.1"]
            ```
        1. [ ] If you have questions about the runner or you're stuck and need help, ask in `#g_runner` Slack channel.

    </details>


2. [ ] [GCK (Gitlab Compose Kit)](https://gitlab.com/gitlab-org/gitlab-compose-kit) is an alternative development environment for those familiar with Docker. It works better for Linux than macOS.
    1. [ ] Full installation instructions for the GCK can be found [here](https://gitlab.com/gitlab-org/gitlab-compose-kit#use-it).
    1. [ ] The GCK has an automatically configured Gitlab Runner built in, but this might not cooperate with your firewall settings. Consider joining the [#gck](https://app.slack.com/client/T02592416/CDPA9TK1B) slack channel if you require assistance.


### Day 7: Sec playground
Before proceeding with this step, you should have on your local machine:

 * a running instance of GitLab EE (via `gdk start`)
 * a working GitLab CI Runner connected to your local instance.

#### General GitLab Development
1. [ ] [Familiarize yourself with Sentry](https://docs.google.com/presentation/d/1j1J4NhGQEYBY8la6lCK-N-bw749TbcTSFTD-ANHiels/edit). Login and search for 500 errors related to your Stage.

<details>
<summary>Protect</summary>

1. [ ] Create a [new Container Security Technical Onboarding issue](https://gitlab.com/gitlab-org/protect/onboarding/-/issues/new?issuable_template=ContainerSecurity-TechnicalOnboarding) and start working on it.
</details>

<details>
<summary>Secure</summary>

1. [ ] [Review the Secure Data Model Brown Bag](https://gitlab.com/gitlab-org/secure/brown-bag-sessions/-/issues/5) 
1. [ ] [Watch the follow-up code walk-through](https://www.youtube.com/watch?v=iiYrvEPT5Fo).
1. [ ] [Watch the Security Report Parsing and Ingestion Brown Bag](https://youtu.be/1NhjpXw0UFI)

#### Security Reports Overview
Importing and running CI pipelines on this sample project will populate your security report dashboard.

1. [ ] On your local instance of GitLab, [import by URL](https://docs.gitlab.com/ee/user/project/import/repo_by_url.html) the [Security Reports examples](https://gitlab.com/gitlab-examples/security/security-reports) repository. You may need to generate a [Personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) in your GitLab.com account with the `read_repository` scope, provide the credentials in the **Username** and **Password** optional fields, and use the `https://` Git URL.
1. [ ] Add this project to a [group](https://docs.gitlab.com/ee/user/group/#add-projects-to-a-group).
1. [ ] Kick off the CI process (pipeline) by following the [project README](https://gitlab.com/gitlab-examples/security/security-reports/blob/master/README.md).
1. [ ] Check the `Security` tab of the pipeline results, where you should see a list of vulnerabilities.
1. [ ] Inspect the `.gitlab-ci.yml` file and understand how the project is configured to generate reports. Feel free to dig into the example projects referenced within the file.

#### SAST
1. [ ] On your local instance of GitLab, [import by URL](https://docs.gitlab.com/ee/user/project/import/repo_by_url.html) a [test SAST project](https://gitlab.com/gitlab-org/security-products/tests?tag=SAST) of your choosing.
1. [ ] Add this project to a [group](https://docs.gitlab.com/ee/user/group/#add-projects-to-a-group). It will allow you to enable the Security dashboard correctly.
1. [ ] Create and run a pipeline for the `master` branch of the imported project.
1. [ ] Learn more about [SAST environment variables](https://docs.gitlab.com/ee/user/application_security/sast/#available-cicd-variables) and how they affect the analyzers.
1. [ ] Once you have a green pipeline, you should be able to see that a security report has been uploaded as an artifact (i.e. `gl-sast-report.json`. This report will be used to populate the Security dashboard.
1. [ ] Explore the `Security` tab in the pipeline page.
       When clicking on a vulnerability, GitLab shows a modal window with more information (like the file
       where the vulnerability has been found) and actions (dismiss and create an issue).
       Note: there must be an existing security report artifact generated by the
       pipeline in order for that tab to exist. Artifacts are generally deleted
       after some time, so old pipelines won't have a `Security` tab.

#### DAST

**Getting to know ZAP**
<details>
<summary>Steps</summary>

DAST is GitLab's dynamic analysis tool. It leverages ZAP, a piece of open-source software used to find vulnerabilities in running websites.

To get to know ZAP do the following steps:

1. [ ] Download and run locally [railsgoat](https://github.com/OWASP/railsgoat) or [nodegoat] (https://github.com/OWASP/NodeGoat).

1. [ ] Download and install [ZAP](https://www.zaproxy.org/download/)

1. [ ] Run an Passive Scan against your locally running RailsGoat / NodeGoat using the instructions in [zap-in-10](https://www.zaproxy.org/zap-in-ten/)

</details>

**Configuring your local machine to build DAST**
<details>
<summary>Steps</summary>

Assumed: Docker is installed, and you are comfortable using it.

1. [ ] DAST uses Python, install [pyenv](https://github.com/pyenv/pyenv#homebrew-on-macos)
1. [ ] Install DAST specific dependencies `brew install jq shellcheck`
1. [ ] Check out the DAST codebase `git clone git@gitlab.com:gitlab-org/security-products/dast.git`, `cd` into the cloned directory
1. [ ] Install DAST's version of Python `pyenv install $(cat .python-version)`
1. [ ] Install bash_unit `bash <(curl -s https://raw.githubusercontent.com/pgrange/bash_unit/master/install.sh) && mv ./bash_unit /usr/local/bin`
1. [ ] Install Python package dependencies `pip install -r requirements-test.txt`
1. [ ] Build a local DAST image `docker build -t dast .`
1. [ ] Make sure that you can run tests `invoke test.unit lint.python && bash_unit ./test/end-to-end/test-baseline.sh

</details>


**Viewing DAST vulnerabilities in the Security Dashboard**
<details>
<summary>Steps</summary>

Assumed: Docker installed, and you are comfortable using it, GDK started, license is installed and a runner configured and working.

1. [ ] Create a new project
1. [ ] From the project home page, click the `Web IDE` button. Click the `+` on the left pane for new file, and create a `.gitlab-ci.yml` file. It should have the following contents:
    ```yml
    stages:
      - build

    dast:
       stage: build
       image: alpine:3.11.3
       script:
       - apk add curl
       - curl https://gitlab.com/gitlab-org/security-products/dast/-/raw/main/test/end-to-end/expect/test_webgoat_full_scan.json > gl-dast-report.json
       artifacts:
         reports:
           dast: gl-dast-report.json
    ```
1. [ ] Click `Commit`. Type 'Add GitLab CI configuration' as the message, ensure you commit to `master`, and click `Commit` again.
1. [ ] In your project home page, click `CI / CD -> Pipelines`. You should have a new Pipeline running your new build/dast job.
1. [ ] When the Pipeline has completed successfully, click on `Security & Compliance -> Security Dashboard`. You should see all of the vulnerabilities in `test_webgoat_full_scan.json` in the Security Dashboard.

</details>

**Running locally built DAST in your local GitLab**
<details>
<summary>Steps</summary>

Assumed: Docker installed, and you are comfortable using it. Your local machine has been configured to build DAST. GDK started, runner configured and is working
1. [ ] Navigate in a terminal to the DAST code directory
1. [ ] Start a web server `docker run --rm -p 6060:80 -v "${PWD}/test/end-to-end/fixtures/ajax-spider":/usr/share/nginx/html:ro -v "${PWD}/test/end-to-end/fixtures/ajax-spider/nginx.conf":/etc/nginx/conf.d/default.conf -d nginx`
1. [ ] Verify it works by running `curl http://localhost:6060/food.html`
1. [ ] Find your local IP address. On a MAC, it may be the `en0` IPv4 address found when running `ifconfig`. E.g. `192.168.40.150`
1. [ ] Verify that you can hit your running web server using your IP address `curl http://[IP ADDRESS]:6060/food.html`
1. [ ] In the DAST code directory on your machine in a terminal, build the docker image `docker build -t dast .`
1. [ ] Run a Docker Registry server `docker run -d -p 5001:5000 --restart=always --name registry registry:2` ([more information](https://docs.docker.com/registry/deploying/))
1. [ ] Tag your local dast image with the registry `docker tag dast localhost:5001/dast:1.0.0`
1. [ ] Push your local dast image to the registry `docker push localhost:5001/dast:1.0.0`
1. [ ] In your local running GitLab instance, create a new project. The project will need to have access to a runner
1. [ ] From the project home page, go to the `Web IDE` and create a `.gitlab-ci.yml` file. It should have the following contents (make sure you substitute the IP address):

    ```yml
    stages:
      - build

    dast:
      stage: build
      image: localhost:5001/dast:1.0.0
      script:
        - /analyze -t http://[IP ADDRESS]:6060/food.html
      artifacts:
        reports:
          dast: gl-dast-report.json
    ```

1. Commit to the master branch. A new pipeline should run, adding vulnerabilities to the Security Dashboard

*Optional extensions*
- Enable a full scan by using the variable `DAST_FULL_SCAN_ENABLED: "true"`.
- To see all the `analyze` commandline options, replace the `script` section with `/analyze --help`.
- By default the `analyze` command spiders the site using the HTML spider. Since parts of the food.html page are built using Javascript, change the `analyze` command to use the AJAX spider using the `-j` argument: `/analyze -j -t http://[IP ADDRESS]:6060/food.html`
- Add `paths:[gl-dast-report.json]` under the `artifacts:` configuration if you want the JSON file to be published as an artifact during the Job so you can inspect it
- Replace the locally built DAST image with the production image `registry.gitlab.com/gitlab-org/security-products/dast` (note: image is about 1.5GB).
- Run [railsgoat](https://github.com/OWASP/railsgoat) (follow the instructions on the README for running inside Docker) and run a DAST scan against it using the instructions present [here](https://gitlab.com/gitlab-org/security-products/dast#how-to-use) (in the `docker run` command point it to your instance of railsgoat)

</details>

**DAST Repositories**
<details>
<summary>Information</summary>
[View this document](https://docs.google.com/document/d/17d15ZG49RTOrGpsPAroEpZMdWSkTLw6d9grsbW5lAxQ/edit?usp=sharing) and recording for a walkthrough of the DAST repositories. 

</details>

#### Composition Analysis

1. [ ] [Feature Overview video summary](https://www.youtube.com/watch?v=mZvehoa4JdU&list=PL05JrBw4t0Kq7yUrZazEF3diazV29RRo1&index=7&t=0s). Contents include: Dependency Scanning, Container Scanning, License Management, Vulnerability DB, Analyzers, and data sources (DB and artifacts).

#### Threat Insights

<details>
<summary>Click to expand/contract</summary>

##### Create special case vulnerabilities

Sometimes one needs a vulnerability that can be resolved by a Merge Request on the vulnerability
page; unfortunately not all vulnerabilities have this capability. Here is a way to produce the
necessary data to use this feature.

1. [ ] Clone the [Yarn Remediation](https://staging.gitlab.com/secure-team-test/yarn-remediation) repo
1. [ ] Run a pipeline on the `cureable` branch.
1. [ ] Navigate to the project security dashboard select a vulnerability and view the details page. The
   `Resolve with MR` button should be available.
1. [ ] Click on the `Resolve with MR` button. A Merge Request is created and you are navigated to that page.

</details>


#### AutoDevOps

GitLab can automatically configure the entire CI/CD pipeline for projects (without the need to create the `.gitlab-ci.yml` file), this feature is known as [AutoDevOps](https://docs.gitlab.com/ee/topics/autodevops/index.html)
As part of this task you need to:
1. [ ] Create a new project on GitLab.com and create Kubernetes cluster for your project by following this [guide](https://gitlab.com/help/topics/autodevops/quick_start_guide.md#creating-a-new-project-from-a-template), your GitLab google account should already have access to the `gitlab-demos` and `group-secure` [GCP](https://cloud.google.com) projects; create a cluster in the `group-secure` project.
1. [ ] Install the required Kubernetes plugins as mentioned in the documentation linked above.
1. [ ] In your project go to ` Settings > CI/CD > Auto DevOps` and enable it. This should automatically spawn a new pipeline.
1. [ ] Make sure your pipeline (`CI/CD > Pipelines`) has an `Auto DevOps` label on it, it has all the Secure section jobs in it and that it eventually passes.

</details>


#### Prepare to contribute

1. [ ] Configure GPG to [sign your commits](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/).
1. [ ] Create an MR with improvements to this [document](https://gitlab.com/gitlab-org/secure/onboarding/blob/master/.gitlab/issue_templates/Technical_Onboarding.md).
1. [ ] Note the existence and location of the [security issue workflow](https://about.gitlab.com/handbook/engineering/workflow/#security-issues). You are unlikely to work on a security fix as your first task, but it's important to know that security MRs have a different workflow.

#### Understand GitLab Infrastructure Better
1. [ ] [Review GitLab Monitoring Tools and test them out](https://www.youtube.com/playlist?list=PL05JrBw4t0KpQMEbnXjeQUA22SZtz7J0e)
1. [ ] [Review Visualization Tools and test them](https://www.youtube.com/playlist?list=PL05JrBw4t0KrDIsPQ68htUUbvCgt9JeQj)
If you can not access the above playlists because you get 'This playlist is private.', go to the top right of the screen in Youtube and click 'Switch Account' > 'GitLab Unfiltered'

### Now you are ready for new tasks
You're awesome!

## Troubleshooting

### Security Dashboard won't show vulnerabilities

TLDR: Re-run the pipeline on the default branch

There is currently a distinction between the data that populates the project
security dashboard (PSD) and group security dashboard (GSD).

The PSD pulls vulnerabilities directly from the last pipeline on the default branch
(usually `master`). This requires the last pipeline to have been ran and to have
uploaded a security report.

The GSD pulls vulnerabilities directly from the database, as `Vulnerabilities::Occurrence`
records. This requires the last pipeline to have been ran on the default branch
and been processed through our CI parsers.
